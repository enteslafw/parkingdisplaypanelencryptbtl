/******************************************************************************
 *
 *                Microchip Memory Disk Drive File System
 *
 ******************************************************************************
 * FileName:        HardwareProfile.h
 * Dependencies:    None
 * Processor:       PIC18/PIC24/dsPIC30/dsPIC33/PIC32
 * Compiler:        C18/C30/C32
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the �Company�) for its PICmicro� Microcontroller is intended and
 * supplied to you, the Company�s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
*****************************************************************************/


#ifndef _SPICONFIG_H_
#define _SPICONFIG_H_

//include "..//plexitech_m2m.X/system.h"  //added by umesh as some variables are already defined in this file
// Select your interface type
// This library currently only supports a single physical interface layer at a time


// Description: Macro used to enable the SD-SPI physical layer (SD-SPI.c and .h)
#define USE_SD_INTERFACE_WITH_SPI

/*********************************************************************/
/******************* Pin and Register Definitions ********************/
/*********************************************************************/

/* SD Card definitions: Change these to fit your application when using
   an SD-card-based physical layer                                   */

#ifdef USE_SD_INTERFACE_WITH_SPI

    #if defined (__PIC32MX__)

    #define USB_USE_MSD
    #define USE_SD_INTERFACE_WITH_SPI

    //SPI Configuration
    #define SPI_START_CFG_1     (SPI_OPEN_MSTEN | SPI_OPEN_MODE8 | SPI_OPEN_CKE_REV | SPI_OPEN_ON)
    #define SPI_START_CFG_2     (GetPeripheralClock() / SPI_FREQUENCY)

    // Define the SPI frequency
    #define SPI_FREQUENCY			(10000000)

    #define SD_CS				PORTDbits.RD9 //PORTCbits.RC1
    #define SD_CS_TRIS                          TRISDbits.TRISD9 //TRISCbits.TRISC1

//    #define SD_CD				PORTFbits.RF0
//    #define SD_CD_TRIS			TRISFbits.TRISF0
//
//    #define SD_WE				PORTFbits.RF1
//    #define SD_WE_TRIS			TRISFbits.TRISF1

    // Registers for the SPI module you want to use
    #define SPICON1				SPI1CON  //SPI3ACON
    #define SPISTAT				SPI1STAT //SPI3ASTAT
    #define SPIBUF				SPI1BUF  //SPI3ABUF
    #define SPISTAT_RBF                         SPI1STATbits.SPIRBF //SPI3ASTATbits.SPIRBF
    #define SPICON1bits                         SPI1CONbits  //SPI3ACONbits
    #define SPISTATbits                         SPI1STATbits //SPI3ASTATbits
    #define SPIENABLE                           SPI1CONbits.ON  //SPI3ACONbits.ON
    #define SPIBRG                              SPI1BRG  //SPI3ABRG
    // Tris pins for SCK/SDI/SDO lines
    // Description: The TRIS bit for the SCK pin
    #define SPICLOCK                            TRISDbits.TRISD10 //TRISFbits.TRISF13
    // Description: The TRIS bit for the SDI pin
    #define SPIIN                               TRISCbits.TRISC4  //TRISFbits.TRISF4
    // Description: The TRIS bit for the SDO pin
    #define SPIOUT                              TRISDbits.TRISD0  //TRISFbits.TRISF5

    #define putcSPI(data)                       SpiChnPutC(SPI_CHANNEL1, data)  //SPI_CHANNEL3A
    #define getcSPI()                           SpiChnGetC(SPI_CHANNEL1)  //SPI_CHANNEL3A
    #define OpenSPI(config1, config2)		SpiChnOpen(SPI_CHANNEL1, config1, config2)  //SPI_CHANNEL3A

    // Will generate an error if the clock speed is too low to interface to the card
    #if (GetSystemClock() < 100000)
        #error Clock speed must exceed 100 kHz
    #endif

    #endif

#endif

#endif
