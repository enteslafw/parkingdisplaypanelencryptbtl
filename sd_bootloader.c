/********************************************************************
 FileName:     SD Bootloader.c
 Dependencies: See INCLUDES section
 Processor:		PIC32 USB Microcontrollers
 Hardware:		
 Complier:  	Microchip C18 (for PIC18), C30 (for PIC24), C32 (for PIC32)
 Company:		Microchip Technology, Inc.

 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the ÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½CompanyÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½) for its PICÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½ Microcontroller is intended and
 supplied to you, the CompanyÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½s customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN ÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½AS ISÃÆÃÂ¯ÃâÃÂ¿ÃâÃÂ½ CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

 ********************************************************************
 File Description:

 Change History:
  Rev   Description
  1.0   Initial release
  2.1   Updated for simplicity and to use common
                     coding style
 ********************************************************************/
#include <plib.h>
#include <string.h>
//#include "FSIO.h"
#include "Compiler.h"
#include "NVMem.h"
#include "sd_bootloader.h"
#include "Bootloader.h"
#include "HardwareProfile.h"
#include "fatfs/diskio.h"
#include "fatfs/ff.h"
#include "fatfs/ffconf.h"
#include "XTEA.h"
// *****************************************************************************
// *****************************************************************************
// Device Configuration Bits (Runs from Aux Flash)
// *****************************************************************************
// *****************************************************************************
// Configuring the Device Configuration Registers
// 80Mhz Core/Periph, Pri Osc w/PLL, Write protect Boot Flash
//#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
//#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1
//#pragma config ICESEL = ICS_PGx2, BWP = OFF
//this is as per m2m board
//#pragma config ICESEL = ICS_PGx1, FPLLODIV = DIV_1, FPLLMUL = MUL_24, FPLLIDIV = DIV_4, FWDTEN = OFF, WDTPS = PS128, FPBDIV = DIV_8, POSCMOD = HS, FNOSC = PRIPLL, CP = OFF
//this is as per mifare ethernet board
//#pragma config FPLLODIV = DIV_1, FPLLMUL = MUL_20, FPLLIDIV = DIV_3, FWDTEN = OFF, FPBDIV = DIV_1, POSCMOD = HS, FNOSC = PRIPLL, CP = OFF, ICESEL = ICS_PGx1, DEBUG = ON
#pragma config FPLLODIV = DIV_1, FPLLMUL = MUL_20, FPLLIDIV = DIV_3, FWDTEN = OFF, FPBDIV = DIV_1, POSCMOD = HS, FNOSC = PRIPLL, CP = OFF, ICESEL = ICS_PGx1, DEBUG = ON
#pragma config UPLLEN   = ON , UPLLIDIV = DIV_3   
#pragma config FSOSCEN = OFF

#if defined(TRANSPORT_LAYER_ETH)
#pragma config FMIIEN = OFF, FETHIO = OFF	// external PHY in RMII/alternate configuration
#endif

#define SWITCH_PRESSED 0
#define UART_DEBUG 
//#define LED_DEBUG

#define LED1_PIN_DIR _TRISB11  //_TRISD2
#define LED2_PIN_DIR _TRISB10  //_TRISD3
#define LED3_PIN_DIR _TRISB9
#define LED4_PIN_DIR _TRISB8

#define LED1_PIN _LATB11 //_LATD2
#define LED2_PIN _LATB10  //_LATD3
#define LED3_PIN _LATB9
#define LED4_PIN _LATB8

#define LED1_ON() LED1_PIN=1;
#define LED2_ON() LED2_PIN=1;
#define LED3_ON() LED3_PIN=1;
#define LED4_ON() LED4_PIN=1;

#define LED1_OFF() LED1_PIN=0;
#define LED2_OFF() LED2_PIN=0;
#define LED3_OFF() LED3_PIN=0;
#define LED4_OFF() LED4_PIN=0;


//#define FIRMWARE_FILENAME               "ethioc.hex"  //this name is common with bootloader program
/******************************************************************************
Macros used in this file
 *******************************************************************************/
#define SWITCH_PRESSED 0
#define AUX_FLASH_BASE_ADRS				(0x7FC000)
#define AUX_FLASH_END_ADRS				(0x7FFFFF)
#define DEV_CONFIG_REG_BASE_ADDRESS 	(0xF80000)
#define DEV_CONFIG_REG_END_ADDRESS   	(0xF80012)

/******************************************************************************
Global Variables
 *******************************************************************************/
const char FIRMWARE_FILENAME[] = {"endisp.img"}; //"riots.hex"//"ENLOGGER.hex"
const char PRE_FIRMWARE_FILENAME[] = {"endisp1.img"};
const char DEFAULT_FIRMWARE[] = {"Dendisp.img"};
const char DUMMY_FILENAME[] = {"dummy.img"};
const char *FirmwareFileName;
unsigned int firmwareFileUse = 0; //new firmware

DSTATUS dstatus;
FRESULT res; /* FatFs function common result code */
static FATFS Fatfs;
FIL fsrc, fsrc1; /* file objects */

//FSFILE * myFile;
BYTE myData[512];
size_t numBytes;
UINT pointer = 0;
UINT readBytes, decryptedBytes, numOfBytesWritten;
UINT8 decryptBuffer[1024]; //size must be greater than 520
UINT8 asciiBuffer[1024];
UINT8 asciiRec[200];
UINT8 hexRec[100];

T_REC record;

/****************************************************************************
Function prototypes
 *****************************************************************************/
#ifdef __cplusplus
extern "C"
{
#endif
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec);
void InitializeBoard(void);
BOOL CheckTrigger(void);
void JumpToApp(void);
BOOL ValidAppPresent(void);
#ifdef __cplusplus
}
#endif

#ifdef UART_DEBUG

void Uart4Init(unsigned int baud)
{
    _TRISD15 = 0;
    _TRISD14 = 1;
    UARTConfigure(UART4, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART4, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART4, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART4, GetPeripheralClock(), baud);
    UARTEnable(UART4, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
}

void Uart4PutCh(char txCh)
{
    while (!UARTTransmitterIsReady(UART4));
    UARTSendDataByte(UART4, txCh);
    while (!UARTTransmissionHasCompleted(UART4));
}

char Uart4GetCh(void)
{
    while (!UARTReceivedDataIsAvailable(UART4));
    return UARTGetDataByte(UART4);
}

void Uart4PutS(char *s)
{
    while (*s)
    {
        Uart4PutCh(*s);
        s++;
    }
}
#endif

void DelayMs(unsigned int delayMs)
{
    volatile unsigned int i;
    while (delayMs > 0)
    {
        for (i = 0; i < 8000; i++);
        delayMs--;
    }
}

void InitIO(void)
{
    AD1CHS = 0;
    AD1PCFG = 0xFFFFFFFF; // All pins digital io
    PORTA = 0;
    PORTB = 0;
    PORTC = 0;
    PORTD = 0;
    PORTE = 0;
    PORTF = 0;
    PORTG = 0;

    LATA = 0;
    LATB = 0;
    LATC = 0;
    LATD = 0;
    LATE = 0;
    LATF = 0;
    LATG = 0;

    TRISA = 0;
    TRISB = 0;
    TRISC = 0;
    TRISD = 0;
    TRISE = 0;
    TRISF = 0;
    TRISG = 0;

    _TRISG6 = 0;
    _TRISG7 = 1;
    _TRISG8 = 0;
    _TRISG9 = 0;
}

void Error(int led_no)
{
    int i = 0;

    while (1)
    {
#ifdef LED_DEBUG
        switch (led_no)
        {
        case 0: LED1_OFF();
            LED2_OFF();
            LED3_OFF();
            LED4_OFF();
            break;
        case 1: LED1_ON();
            LED2_OFF();
            LED3_OFF();
            LED4_ON();
            break;
        case 2: LED1_OFF();
            LED2_ON();
            LED3_ON();
            LED4_OFF();
            break;
        case 3: LED1_ON();
            LED2_ON();
            LED3_ON();
            LED4_ON();
            break;

        case 4: LED1_OFF();
            LED2_ON();
            LED3_OFF();
            LED4_OFF();
            break;
            //        case 5: LED1_OFF(); LED2_ON(); LED3_OFF(); LED4_ON();
            //             break;
            //        case 6: LED1_OFF(); LED2_ON(); LED3_ON(); LED4_OFF();
            //             break;
            //        case 7: LED1_OFF(); LED2_ON(); LED3_ON(); LED4_ON();
            //             break;
            //
            //        case 8: LED1_ON(); LED2_OFF(); LED3_OFF(); LED4_OFF();
            //             break;
            //        case 9: LED1_ON(); LED2_OFF(); LED3_OFF(); LED4_ON();
            //             break;
            //        case 10: LED1_ON(); LED2_OFF(); LED3_ON(); LED4_OFF();
            //             break;
            //        case 11: LED1_ON(); LED2_OFF(); LED3_ON(); LED4_ON();
            //             break;
            //
            //        case 12: LED1_ON(); LED2_ON(); LED3_OFF(); LED4_OFF();
            //             break;
            //        case 13: LED1_ON(); LED2_ON(); LED3_OFF(); LED4_ON();
            //             break;
        case 14: LED1_ON();
            LED2_ON();
            LED3_ON();
            LED4_OFF();
            break;
        case 15: LED1_ON();
            LED2_ON();
            LED3_ON();
            LED4_ON();
            break;
        }
        DelayMs(1000);
        LED1_OFF();
        LED2_OFF();
        LED3_OFF();
        LED4_OFF();
        DelayMs(1000);
#else
        DelayMs(1000);
        mBUZZER ^= 1;
#endif
        i++;
        if (i >= 5)
        {
            Reset();
        }
    }
}

/********************************************************************
 * Function: 	main()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		None.
 *
 * Side Effects:	None.
 *
 * Overview: 	Main entry function. If there is a trigger or 
 *				if there is no valid application, the device 
 *				stays in firmware upgrade mode.
 *
 *			
 * Note:		 	None.
 ********************************************************************/
int main(void)
{
    volatile UINT i;
    volatile BYTE led = 0;

#ifndef PIC32_STARTER_KIT
    /*The JTAG is on by default on POR.  A PIC32 Starter Kit uses the JTAG, but
    for other debug tool use, like ICD 3 and Real ICE, the JTAG should be off
    to free up the JTAG I/O */
    DDPCONbits.JTAGEN = 0;
#endif

    /*Refer to the C32 peripheral library documentation for more
     information on the SYTEMConfig function.
     Configure cache, wait states and peripheral bus clock
     Configure the device for maximum performance but do not change the PBDIV
     Given the options, this function will change the flash wait states, RAM
     wait state and enable prefetch cache but will not change the PBDIV.
     The PBDIV value is already set via the pragma FPBDIV option*/
    // Setup configuration
    //SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    WDTCONbits.ON = 0; //disable watchdog timer

    // Enable multi-vectored interrupts
    INTEnableSystemMultiVectoredInt();

    // Enable optimal performance
    SYSTEMConfigPerformance(GetSystemClock());
    mOSCSetPBDIV(OSC_PB_DIV_1); // Use 1:1 CPU Core:Peripheral clocks


    InitIO();

    CloseI2C2();
    DisableIntSI2C2; //Disable I2C interrupt
#ifdef LED_DEBUG
    InitLED();
#endif
    LED1_OFF();
    LED2_OFF();
    LED3_OFF();
    LED4_OFF();

#ifdef UART_DEBUG
    Uart4Init(115200);
#endif
#ifdef LED_DEBUG
    BlinkLED();
#else
    _LATD1 = 0;
    _TRISD1 = 0; // buzzer in mifare ethernet
    Buzzer();
#endif
    //    if(!CheckTrigger() && ValidAppPresent())
    //	{
    //        // This means the switch is not pressed. Jump
    //        // directly to the application
    //
    //        JumpToApp();        
    //    }
    //#ifdef UART_DEBUG
    //    Uart4PutS("test1\r\n");
    //#endif

#if   (((__PIC32_FEATURE_SET__ >= 100) && (__PIC32_FEATURE_SET__ <= 299)))
#error("TODO: For PIC32MX1xx/PIC32MX2xx devices, user must map the SPI ports to required I/Os using PPS");
    /* Example Code
    PPSInput(3,SDI2,RPn); // SDI2 mapping, where RPn = RPA2, RPB6....
    	
    PPSOutput(2,RPn,SDO2);// SDO2 on RPA8
    
    //Do not forget to switch-off corrresponding "analog selection".
    ANSELx = 0;
     */
#endif

    DelayMs(4000);
    //    myFile = NULL;
#ifdef UART_DEBUG  
    Uart4PutS("Running SD Bootloader\r\n");
#endif
    dstatus = disk_initialize(0);
    if ((dstatus & STA_NOINIT))
    {
        Nop();
        Nop();
#ifdef UART_DEBUG
        Uart4PutS("SD CARD INIT ERROR!\r\n");
#endif
        Error(14);
    }

    if (FR_OK != f_mount(0, &Fatfs))
    {
#ifdef UART_DEBUG
        Uart4PutS("SD CARD MOUNT ERROR!\r\n");
#endif        
        Nop();
        Nop();

        Error(1);
    }

    //finding if previous written firmware file is deleted by main app, this will tell btl that previous writing is successful.
    res = f_open(&fsrc, PRE_FIRMWARE_FILENAME, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {
        //file not found. that means previous written is successful
        FirmwareFileName = &FIRMWARE_FILENAME[0];
        firmwareFileUse = 0; //new firmware
    }
    else
    {
        //file found
        //something went wrong, main app not started in previous upgrade
        //re-write default firmware or last successful firmware available.
#ifdef UART_DEBUG
        Uart4PutS("previous session failed\r\n");
        Uart4PutS("loading last working firmware\r\n");
#endif
        f_close(&fsrc);
        f_unlink(PRE_FIRMWARE_FILENAME);
        FirmwareFileName = &DEFAULT_FIRMWARE[0];
        firmwareFileUse = 1; //default firmware
    }

    res = f_open(&fsrc, FirmwareFileName, FA_OPEN_EXISTING | FA_READ);
    if (res != FR_OK)
    {//if new file not available, check for valid firmware entry
        if (ValidAppPresent())
        {//if found valid firmware written, start it
            JumpToApp();
        }
        else
        {
            //            f_rename(DEFAULT_FIRMWARE, FIRMWARE_FILENAME);
            //            f_open(&fsrc, PRE_FIRMWARE_FILENAME, FA_CREATE_ALWAYS | FA_WRITE);
            //            f_close(&fsrc);
            Uart4PutS("No valid app, loading default firmware\r\n");
            res = f_open(&fsrc, DEFAULT_FIRMWARE, FA_OPEN_EXISTING | FA_READ);
            if (res != FR_OK)
            {
                Uart4PutS("No default firmware\r\n");
                Error(2);
                //we shouldn't reach here.
            }
            firmwareFileUse = 1; //default firmware
        }
    }
#ifdef UART_DEBUG
    else
    {
        Uart4PutS("loading New firmware\r\n");
    }
#endif


    //#ifdef UART_DEBUG	   	
    //    Uart4PutS("test3\r\n");
    //    Uart4PutS("test4\r\n");
    //    Uart4PutS("test6\r\n");
    //#endif
    // Erase Flash (Block Erase the program Flash)
    EraseFlash();
    // Initialize the state-machine to read the records.
    record.status = REC_NOT_FOUND;

    while (1)
    {
        //Uart4PutS("test7\r\n");
        //read encrypted file, decrypt it and write in dummy file. this dummy file now use as a firmware file for further process
        //        res = f_open(&fsrc1, DUMMY_FILENAME, FA_CREATE_ALWAYS | FA_WRITE);
        //        if (res != FR_OK)
        //        {
        //            Nop();
        //            Error(3);
        //        }
        //        while (1)
        //        {
        //            res = f_read(&fsrc, (void*) asciiBuffer, 520, &readBytes);
        //            if (res != FR_OK)
        //            {
        //                f_close(&fsrc1);
        //                f_close(&fsrc);
        //                Error(3);
        //            }
        //            else if (readBytes > 0)
        //            {
        //                //decrypt read buffer and store into file
        //                decryptBlock((uint32_t*) asciiBuffer, &readBytes, TEAKey);
        //                res = f_write(&fsrc1, asciiBuffer, readBytes, &numOfBytesWritten);
        //                if ((res != FR_OK) || (numOfBytesWritten != readBytes))
        //                {
        //                    f_close(&fsrc1);
        //                    f_close(&fsrc);
        //                    Error(3);
        //                }
        //            }
        //            else
        //            {
        //                //file read completed
        //                f_close(&fsrc1);
        //                f_close(&fsrc);
        //                break;
        //            }
        //        }
        // For a faster read, read 512 bytes at a time and buffer it.
        //        readBytes = FSfread((void *) &asciiBuffer[pointer], 1, 512, myFile);
        //res = f_read(&fsrc1, (void*) &asciiBuffer[pointer], 512, &readBytes);
        res = f_read(&fsrc, (void*) decryptBuffer, 520, &readBytes); //do not alter 520

        //        if (readBytes == 0)
        if ((readBytes == 0) || (res != FR_OK))
        {
#ifdef UART_DEBUG             
            Uart4PutS("The hex file has ended abruptly\r\n");
#endif
            // Nothing to read. Come out of this loop
            // break;
            //            FSfclose(myFile);
            // Something fishy. The hex file has ended abruptly, looks like there was no "end of hex record".
            //Indicate error and stay in while loop.
            Error(3);
            while (1);
        }
        decryptedBytes = readBytes;
        decryptBlock((uint32_t*) decryptBuffer, &decryptedBytes, TEAKey);

        //        if (decryptedBytes > 0)
        if (readBytes == ((decryptedBytes + 7) / 8) * 8 + 8)
        {
            strncpy(&asciiBuffer[pointer], decryptBuffer, decryptedBytes);
        }
        else
        {
            //failed to decrypt, we shouldn't reach here
#ifdef UART_DEBUG             
            Uart4PutS("failed to decrypt\r\n");
#endif
            //delete file
            f_close(&fsrc); //file closed
            f_unlink(FirmwareFileName);
            Error(4);
        }

        for (i = 0; i < (decryptedBytes + pointer); i++)
        {

            // This state machine seperates-out the valid hex records from the read 512 bytes.
            switch (record.status)
            {
            case REC_FLASHED:
            case REC_NOT_FOUND:
                if (asciiBuffer[i] == ':')
                {
                    // We have a record found in the 512 bytes of data in the buffer.
                    record.start = &asciiBuffer[i];
                    record.len = 0;
                    record.status = REC_FOUND_BUT_NOT_FLASHED;
                }
                break;
            case REC_FOUND_BUT_NOT_FLASHED:
                if ((asciiBuffer[i] == 0x0A) || (asciiBuffer[i] == 0xFF))
                {
                    // We have got a complete record. (0x0A is new line feed and 0xFF is End of file)
                    // Start the hex conversion from element
                    // 1. This will discard the ':' which is
                    // the start of the hex record.
                    ConvertAsciiToHex(&record.start[1], hexRec);
                    WriteHexRecord2Flash(hexRec);
                    record.status = REC_FLASHED;
                }
                break;
            }
            // Move to next byte in the buffer.
            record.len++;
        }

        if (record.status == REC_FOUND_BUT_NOT_FLASHED)
        {
            // We still have a half read record in the buffer. The next half part of the record is read 
            // when we read 512 bytes of data from the next file read. 
            memcpy(asciiBuffer, record.start, record.len);
            pointer = record.len;
            record.status = REC_NOT_FOUND;
        }
        else
        {
            pointer = 0;
        }
        // Blink LED at Faster rate to indicate programming is in progress.
        led += 3;
        mLED = ((led & 0x80) == 0);

    }//while(1)


    return 0;
}

/********************************************************************
 * Function: 	CheckTrigger()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		TRUE: If triggered
                                FALSE: No trigger
 *
 * Side Effects:	None.
 *
 * Overview: 	Checks if there is a trigger to enter 
                                firmware upgrade mode.
 *
 *			
 * Note:		 	None.
 ********************************************************************/
BOOL CheckTrigger(void)
{
    UINT SwitchStatus;
    SwitchStatus = ReadSwitchStatus();
    if (SwitchStatus == SWITCH_PRESSED)
    {
        // Switch is pressed
        return TRUE;
    }
    else
    {
        // Switch is not pressed.
        return FALSE;
    }
}

/********************************************************************
 * Function: 	JumpToApp()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		
 *
 * Side Effects:	No return from here.
 *
 * Overview: 	Jumps to application.
 *
 *			
 * Note:		 	None.
 ********************************************************************/
void JumpToApp(void)
{
#ifdef UART_DEBUG
    Uart4PutS("\r\nStarting Application\r\n");
#endif
    // Disable any interrupts here before jumping to the application.
    INTDisableInterrupts();
    LED1_OFF();
    LED2_OFF();
    LED3_OFF();
    LED4_OFF();
    f_mount(0, NULL);
    //    Error(0);
    void (*fptr)(void);
    fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
    fptr();
}

/********************************************************************
 * Function: 	ConvertAsciiToHex()
 *
 * Precondition: 
 *
 * Input: 		Ascii buffer and hex buffer.
 *
 * Output:		
 *
 * Side Effects:	No return from here.
 *
 * Overview: 	Converts ASCII to Hex.
 *
 *			
 * Note:		 	None.
 ********************************************************************/
void ConvertAsciiToHex(UINT8* asciiRec, UINT8* hexRec)
{
    UINT8 i = 0;
    UINT8 k = 0;
    UINT8 hex;


    while ((asciiRec[i] >= 0x30) && (asciiRec[i] <= 0x66))
    {
        // Check if the ascci values are in alpha numeric range.

        if (asciiRec[i] < 0x3A)
        {
            // Numerical reperesentation in ASCII found.
            hex = asciiRec[i] & 0x0F;
        }
        else
        {
            // Alphabetical value.
            hex = 0x09 + (asciiRec[i] & 0x0F);
        }

        // Following logic converts 2 bytes of ASCII to 1 byte of hex.
        k = i % 2;

        if (k)
        {
            hexRec[i / 2] |= hex;

        }
        else
        {
            hexRec[i / 2] = (hex << 4) & 0xF0;
        }
        i++;
    }

}
// Do not change this
#define FLASH_PAGE_SIZE 0x1000

/********************************************************************
 * Function: 	EraseFlash()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		
 *
 * Side Effects:	No return from here.
 *
 * Overview: 	Erases Flash (Block Erase).
 *
 *			
 * Note:		 	None.
 ********************************************************************/
void EraseFlash(void)
{
    void * pFlash;
    UINT result;
    INT i;

    pFlash = (void*) APP_FLASH_BASE_ADDRESS;
    for (i = 0; i < ((APP_FLASH_END_ADDRESS - APP_FLASH_BASE_ADDRESS + 1) / FLASH_PAGE_SIZE); i++)
    {
        result = NVMemErasePage(pFlash + (i * FLASH_PAGE_SIZE));
        // Assert on NV error. This must be caught during debug phase.

        if (result != 0)
        {
            // We have a problem. This must be caught during the debug phase.
            while (1);
        }
        // Blink LED to indicate erase is in progress.
        mLED = mLED ^ 1;
    }
}

/********************************************************************
 * Function: 	WriteHexRecord2Flash()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		
 *
 * Side Effects:	No return from here.
 *
 * Overview: 	Writes Hex Records to Flash.
 *
 *			
 * Note:		 	None.
 ********************************************************************/
void WriteHexRecord2Flash(UINT8* HexRecord)
{
    static T_HEX_RECORD HexRecordSt;
    UINT8 Checksum = 0;
    UINT8 i;
    UINT WrData;
    UINT RdData;
    void* ProgAddress;
    UINT result;

    HexRecordSt.RecDataLen = HexRecord[0];
    HexRecordSt.RecType = HexRecord[3];
    HexRecordSt.Data = &HexRecord[4];

    // Hex Record checksum check.
    for (i = 0; i < HexRecordSt.RecDataLen + 5; i++)
    {
        Checksum += HexRecord[i];
    }

    if (Checksum != 0)
    {
        //Error. Hex record Checksum mismatch.
        //Indicate Error by switching ON all LEDs.
#ifdef UART_DEBUG            
        Uart4PutS("\r\nHex record Checksum mismatch");
#endif
        //delete file
        f_close(&fsrc); //file closed
        f_unlink(FirmwareFileName);
        Error(15);
        // Do not proceed further.
        while (1);
    }
    else
    {
        // Hex record checksum OK.
        switch (HexRecordSt.RecType)
        {
        case DATA_RECORD: //Record Type 00, data record.
            HexRecordSt.Address.byte.MB = 0;
            HexRecordSt.Address.byte.UB = 0;
            HexRecordSt.Address.byte.HB = HexRecord[1];
            HexRecordSt.Address.byte.LB = HexRecord[2];

            // Derive the address.
            HexRecordSt.Address.Val = HexRecordSt.Address.Val + HexRecordSt.ExtLinAddress.Val + HexRecordSt.ExtSegAddress.Val;

            while (HexRecordSt.RecDataLen) // Loop till all bytes are done.
            {

                // Convert the Physical address to Virtual address. 
                ProgAddress = (void *) PA_TO_KVA0(HexRecordSt.Address.Val);

                // Make sure we are not writing boot area and device configuration bits.
                if (((ProgAddress >= (void *) APP_FLASH_BASE_ADDRESS) && (ProgAddress <= (void *) APP_FLASH_END_ADDRESS))
                        && ((ProgAddress < (void*) DEV_CONFIG_REG_BASE_ADDRESS) || (ProgAddress > (void*) DEV_CONFIG_REG_END_ADDRESS)))
                {
                    if (HexRecordSt.RecDataLen < 4)
                    {

                        // Sometimes record data length will not be in multiples of 4. Appending 0xFF will make sure that..
                        // we don't write junk data in such cases.
                        WrData = 0xFFFFFFFF;
                        memcpy(&WrData, HexRecordSt.Data, HexRecordSt.RecDataLen);
                    }
                    else
                    {
                        memcpy(&WrData, HexRecordSt.Data, 4);
                    }
                    // Write the data into flash.	
                    result = NVMemWriteWord(ProgAddress, WrData);
                    // Assert on error. This must be caught during debug phase.		
                    if (result != 0)
                    {
                        while (1);
                    }
                }

                // Increment the address.
                HexRecordSt.Address.Val += 4;
                // Increment the data pointer.
                HexRecordSt.Data += 4;
                // Decrement data len.
                if (HexRecordSt.RecDataLen > 3)
                {
                    HexRecordSt.RecDataLen -= 4;
                }
                else
                {
                    HexRecordSt.RecDataLen = 0;
                }
            }
            break;

        case EXT_SEG_ADRS_RECORD: // Record Type 02, defines 4th to 19th bits of the data address.
            HexRecordSt.ExtSegAddress.byte.MB = 0;
            HexRecordSt.ExtSegAddress.byte.UB = HexRecordSt.Data[0];
            HexRecordSt.ExtSegAddress.byte.HB = HexRecordSt.Data[1];
            HexRecordSt.ExtSegAddress.byte.LB = 0;
            // Reset linear address.
            HexRecordSt.ExtLinAddress.Val = 0;
            break;

        case EXT_LIN_ADRS_RECORD: // Record Type 04, defines 16th to 31st bits of the data address. 
            HexRecordSt.ExtLinAddress.byte.MB = HexRecordSt.Data[0];
            HexRecordSt.ExtLinAddress.byte.UB = HexRecordSt.Data[1];
            HexRecordSt.ExtLinAddress.byte.HB = 0;
            HexRecordSt.ExtLinAddress.byte.LB = 0;
            // Reset segment address.
            HexRecordSt.ExtSegAddress.Val = 0;
            break;

        case END_OF_FILE_RECORD: //Record Type 01, defines the end of file record.
            HexRecordSt.ExtSegAddress.Val = 0;
            HexRecordSt.ExtLinAddress.Val = 0;

            f_close(&fsrc); //file closed
            //if (f_unlink(FIRMWARE_FILENAME) == FR_OK)
            //            f_unlink(DUMMY_FILENAME);
            if (0 == firmwareFileUse) //new firmware, rename it
            {

                if (f_rename(FirmwareFileName, PRE_FIRMWARE_FILENAME) == FR_OK)
#ifdef UART_DEBUG
                    Uart4PutS("FIRMWARE File Removed\n");
#else
                    {
                        ;
                    }
#endif
            }

            JumpToApp();
            break;

        default:
            HexRecordSt.ExtSegAddress.Val = 0;
            HexRecordSt.ExtLinAddress.Val = 0;
            break;
        }
    }

}

/********************************************************************
 * Function: 	ValidAppPresent()
 *
 * Precondition: 
 *
 * Input: 		None.
 *
 * Output:		TRUE: If application is valid.
 *
 * Side Effects:	None.
 *
 * Overview: 	Logic: Check application vector has 
                                some value other than "0xFFFFFF"
 *
 *			
 * Note:		 	None.
 ********************************************************************/
BOOL ValidAppPresent(void)
{
    volatile UINT32 *AppPtr;

    AppPtr = (UINT32*) USER_APP_RESET_ADDRESS;

    if (*AppPtr == 0xFFFFFFFF)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

/*********************End of File************************************/
