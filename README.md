# README #

README before cloning

### What is this repository for? ###

* This is bootloader program for Parking Display Panel
* Version 1.0


### How do I get set up? ###

* Clone the repository.
* unzip nbproject.zip
* include and place microchip stack folder outside the project folder such that path will be : 
* Project-> BootLoader-> ClonedFolder 
* Project-> Microchip
* if program not compiled then manually add Microchip stack file path in project include list

### Contribution guidelines ###

* create new branch for new changes made. do not push in to same master branch.
* suggestions are welcome.

### Who do I talk to? ###

* Umesh W: umeshw@entesla.com