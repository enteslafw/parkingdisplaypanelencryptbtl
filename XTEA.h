/* ************************************************************************** */
/** XTEA.h

  @Company
 EnTesla, India

  @File Name
    XTEA.h

  @Summary
 Encryption and Decryption method using XTEA 128 bit

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _XTEA_H    /* Guard against multiple inclusion */
#define _XTEA_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    typedef unsigned long uint32_t;
    typedef unsigned int uint8_t;

    extern const uint32_t TEAKey[4];

    void encrypt(uint32_t* v, uint32_t* k);
    void decrypt(uint32_t* v, uint32_t* k);
    void encryptBlock(uint8_t * data, uint32_t * len, uint32_t * key);
    void decryptBlock(uint8_t * data, uint32_t * len, uint32_t * key);

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
